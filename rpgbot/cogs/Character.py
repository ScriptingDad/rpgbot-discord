from rpgbot.db import Players, Languages
from discord.ext import commands


class Character(commands.Cog):
    """
    Agragate of all the Character options a player might want to add.
    Currently only language is supported but I would like to add die rolls and named die rolls.
    """

    def __init__(self, bot):
        self.bot = bot

    @commands.group()
    async def language(self, ctx):
        """
        Provides a way for players to register and manage their supported languages to the server.
        """
        if ctx.invoked_subcommand is None:
            await ctx.send("Invalid command passed.")

    @language.command()
    async def add(self, ctx, language):
        """Provides facility to add languages to a Character."""
        language = language.upper()
        user = Players.find_one({"user": str(ctx.author)})
        if not user:
            user = {
                "user": str(ctx.author),
                "guild": ctx.guild.name,
                "channel": ctx.channel.name,
                "languages": [],
            }
            Players.insert_one(user)
        if not Languages.find_one({"upper": language}):
            await ctx.send(
                "The language {} isn't presently supported.".format(language)
            )
            return
        elif language not in user["languages"]:
            user["languages"].append(language)
        Players.update_one({"user": str(ctx.author)}, {"$set": user})
        await ctx.send(", ".join(user["languages"]))

    @language.command()
    async def remove(self, ctx, language):
        """Allows users to remove one language at a time."""
        language = language.upper()
        _filter = {"user": str(ctx.author)}
        user = Players.find_one(_filter)
        if not user:
            await ctx.send(
                "The player {} has not been registered yet.".format(str(ctx.author))
            )

        if language in user["languages"]:
            user["languages"].remove(language)
            Players.update_one(_filter, {"$set": user})
        else:
            await ctx.send("The language {} wasn`\t present".format(language))
        await ctx.send(", ".join(user["languages"]))

    #

    @language.command(name="list")
    async def _list(self, ctx):
        """List all languages Character has registered."""
        _filter = {"user": str(ctx.author)}
        user = Players.find_one(_filter)
        if not user:
            await ctx.send(
                "{} does not have any languages assigned yet.".format(
                    ctx.author.mention
                )
            )
        await ctx.author.send(
            "{}'s languages are:\n\t{}".format(
                ctx.author.mention, ",\n\t".join(user["languages"])
            )
        )

def setup(bot):
    bot.add_cog(Character(bot))
