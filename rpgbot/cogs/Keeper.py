from rpgbot.db import Players, Languages
from discord.ext import commands


class KeeperControl(commands.Cog):
    '''
    Allows Keepers certain controls like managing a users languages and
    sending messages in different languages.

    Current problem is that I have no clue how to handle permissions.
    One option was to use server permissions but if multiple users 
    had the keeper role then they would both be able to do everything.

    There are no channel based permissions to be used at this time.
    '''

    def __init__(self, bot):
        self.bot = bot

    @commands.group()
    async def keeper(self, ctx):
        """Keeper root for organizeing commands."""
        if ctx.invoked_subcommand is None:
            await ctx.send('Invalid command passed.')

    @keeper.group(name='language')
    async def keeperlanguage(self, ctx):
        """keeper root for language commands."""
        if ctx.invoked_subcommand is None:
            await ctx.send('Invalid command passed.')
    
    @keeperlanguage.command()
    async def Add(self, ctx, player, language):
        """Allow keeper to add a single language to a play."""
        await ctx.send("This feature isn't implemented yet")

    @keeperlanguage.command()
    async def replace(self, ctx, player, language):
        """Allow keeper to completely assign multiple languages at once to one player."""
        await ctx.send("This feature isn't implemented yet")

    @keeperlanguage.command()
    async def remove(self, ctx, player, language):
        """Allow keeper to delete individual languages at once to one player."""
        await ctx.send("This feature isn't implemented yet")

    @keeperlanguage.command(name='list')
    async def _list(self, ctx, player):
        """Allow keeper to assign multiple languages at once to one player."""
        await ctx.send("This feature isn't implemented yet")

    @keeperlanguage.command()
    async def list_all(self, ctx, player):
        """Keeper can print out all users in the channels language."""
        await ctx.send("This feature isn't implemented yet")


def setup(bot):
    bot.add_cog(KeeperControl(bot))
