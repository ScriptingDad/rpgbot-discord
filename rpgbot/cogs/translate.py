from google.cloud import translate
from discord.ext import commands
from discord import DMChannel
from rpgbot.db import Languages, Players
from functools import lru_cache


class Translate(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def message(self, ctx, language, message):
        message = Message(message)
        for player in ctx.channel.members:
            data = Players.find_one({'user': str(player)})
            if data and language.upper() in data['languages']:
                await player.send(message)
        tmessage = message.translate(language)
        await ctx.send(tmessage)
        
    @commands.command()
    async def message_inplace(self, ctx, language, message):
        await ctx.message.delete()

        message = Message(message)
        for player in ctx.channel.members:
            data = Players.find_one({'user': str(player)})
            if data and language.upper() in data['languages']:
                await player.send(message)
        tmessage = message.translate(language)
        await ctx.send(tmessage)

    @commands.command()
    async def message_as_dm(self, ctx, language, message):
        if not isinstance(ctx.channel, DMChannel):
            await ctx.send('You shouldn\'t do that!')
            return
        
        ### TODO: this shouldn't work till I figure out how to get the Channel

        message = Message(message)
        # for player in ctx.channel.members:
        #     data = Players.find_one({'user': str(player)})
        #     if data and language.upper() in data['languages']:
        #         await player.send(message)
        tmessage = message.translate(language)
        await ctx.send(tmessage)

class Message:
    client = translate.Client()

    def __init__(self, message):
        self.message = message

    def get_code(self, language):
        result = None
        if len(language) > 2:
            result = Languages.find_one({'upper': language.upper()})
        else:
            result = Languages.find_one({'language': language})
        
        if result is None:
            raise ValueError('No language {}'.format(language))
        return result['language']

    @lru_cache(maxsize=1000)
    def translate(self, language):
        return self.client.translate(
            self.message,
            target_language=self.get_code(language)
        )['translatedText']

    def __str__(self):
        return self.message

    def __repr__(self):
        return self.message


def setup(bot):
    if (Languages.find({})).count() == 0:
        for lang in translate.get_languages():
            lang['upper'] = lang['name'].upper()
            lang['lower'] = lang['name'].lower()
            Languages.insert_one(lang)

    bot.add_cog(Translate(bot))
