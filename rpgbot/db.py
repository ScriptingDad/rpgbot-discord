from tinymongo import TinyMongoClient

DB = "RPGPyBot"

conn = TinyMongoClient()
db = getattr(conn, DB, "TestDB")
Players = getattr(db, "Players")
Languages = getattr(db, "Languages")
