from discord.ext.commands import Bot
from pathlib import Path

EXTENSIONS = "./rpgbot/cogs"
CMD_PREFIX = ("!",)
TOKEN = "NjAyMzIyODc0MjcxMDA2NzIw.XTPQCw.o_Xl4dd0Yf9Ua345JLK5b9jsZBY"

client = Bot(command_prefix=CMD_PREFIX)


def get_extensions(EXTENSIONS):
    for f in Path(EXTENSIONS).glob("**/*.py"):
        *parts, last = list(list(f.parts))
        parts.append(last.replace(".py", ""))
        yield ".".join(parts)


@client.event
async def on_ready():
    cogs = list(get_extensions(EXTENSIONS))

    for cog in cogs:
        client.load_extension(cog)
    print("Logged in as")
    print(client.user.name)
    print(client.user.id)
    print("-------")


client.run(TOKEN)
