RPGBot

I've been wanting to make something RPG related for a long time but the items I set my sights on where always huge undertakings.
This is something simple that I can build on. Presently it only really translates some text and allows people to register the languages they know. 

In future it might provide Dice rolling as well as saved dice rolls.
Might integrate with a thirdparty character sheet and do away with managing the languages locally.

The API is essentially.

Language Management:

```
!language add French
```

Remove a language:
```
!language remove French
```

List all languages you currently have registered.
```
!language list
```

When the Keeper send a message to users they will use.

```
!message German "Message to be send to that user."
```

There may even be a keeper activation sequence. 

```
!keeper set @userid active
```

The reason for this last one would be to allow the keeper to enable themselve for this game.  After 12 hours you would be removed automatically so if you joined another game you would have limited permissions like all other player. 